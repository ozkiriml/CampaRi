module mod_dis_interface
  ! interface declarations
  real(8) p

  interface
    function euclidean_d ( veci, vecj, nele ) bind ( C, name='euclidean_d_c' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: euclidean_d
    end function euclidean_d
    function euclidean_d_dot ( veci, vecj, nele ) bind ( C, name='euclidean_d_c_dot' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: euclidean_d_dot
    end function euclidean_d_dot
    function euclidean_d_mag ( veci, vecj, nele ) bind ( C, name='euclidean_d_c_mag' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: euclidean_d_mag
    end function euclidean_d_mag
    function maximum_d ( veci, vecj, nele ) bind ( C, name='maximum_d_c' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: maximum_d
    end function maximum_d
    function manhattan_d ( veci, vecj, nele ) bind ( C, name='manhattan_d_c' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: manhattan_d
    end function manhattan_d
    function canberra_d ( veci, vecj, nele ) bind ( C, name='canberra_d_c' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: canberra_d
    end function canberra_d
    function binary_d ( veci, vecj, nele ) bind ( C, name='binary_d_c' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: binary_d
    end function binary_d
    function minkowski_d ( veci, vecj, nele, p ) bind ( C, name='minkowski_d_c' )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: p
      real ( c_double ) :: minkowski_d
    end function minkowski_d

    ! subroutine discEF ( data, res, nr, nc, nbins ) bind ( C, name='discEF' )
    !   use, intrinsic :: iso_c_binding
    !   implicit none
    !   integer ( c_int ) :: nc
    !   integer ( c_int ) :: nr
    !   integer ( c_int ) :: nbins
    !   real ( c_double ) :: data(nr*nc)
    !   integer ( c_int ) :: res(nr*nc)
    ! end subroutine discEF
  end interface

  abstract interface
    function dist_f ( veci, vecj, nele )
      use, intrinsic :: iso_c_binding
      implicit none
      integer ( c_int ) :: nele
      real ( c_double ) :: veci(nele)
      real ( c_double ) :: vecj(nele)
      real ( c_double ) :: dist_f
    end function dist_f
  end interface

contains
  function euclidean_d_for (veci, vecj, nele)
    implicit none
    integer :: nele
    real ( 8 ) :: veci(nele)
    real ( 8 ) :: vecj(nele)
    real ( 8 ) :: euclidean_d_for
    real ( 8 ) :: hlp
    hlp = sum((veci(1:nele) - vecj(1:nele))**2)
    ! euclidean_d_for = sqrt((1.0 nele hlp)/(1.0 nele (vec_len)))
    euclidean_d_for = sqrt(1.0 * hlp)
  end function euclidean_d_for

  function mutinfo ( veci, vecj, nele, ent_met, nbins)
    implicit none
    integer :: nele
    integer :: ent_met
    integer :: nbins
    real (8) :: veci(nele)
    integer :: veci_disc(nele)
    integer :: vecj_disc(nele)
    real (8) :: vecj(nele)
    real (8) :: mutinfo
    ! print *, "asd"
    call discr(veci, veci_disc, nele, nbins)
    call discr(vecj, vecj_disc, nele, nbins)
    mutinfo = entropy(veci_disc, nele, nbins) + entropy(vecj_disc, nele, nbins) - jointentropy(veci_disc, vecj_disc, nele, nbins)
  end function mutinfo

  subroutine discr(a, a_disc, nele, nbins)
    implicit none
    integer :: nele
    real (8) :: a(nele)
    integer :: a_disc(nbins)
    integer :: nbins
    integer :: i
    real(8) :: maxa
    real(8) :: mina
    real(8) :: dela
    maxa = maxval(a)
    mina = minval(a)
    dela = abs(maxa - mina)/(1.0*nbins-1)
    do i=1,nele,1
      a_disc(i) = ceiling((a(i) - mina)/dela)
    end do
  end subroutine discr

  function entropy(a, nele, nbins)
    implicit none
    integer nele
    integer a(nele)
    integer i
    integer nbins
    real(8) :: entropy
    real(8) :: freqs(nbins)
    entropy = 0
    do i=1,nbins,1
      freqs(i) = count(a == i)*1.0/nele
      if (freqs(i) .gt. 0.00000001) then
        entropy = entropy - freqs(i)*log(freqs(i))
      end if
    end do
  end function entropy

function jointentropy(a, b, nele, nbins)
  implicit none
  integer nele
  integer a(nele)
  integer b(nele)
  integer nbins
  integer ab(2*nele)
  real(8) :: jointentropy

  ab(1:nele) = a
  ab((nele+1):(2*nele)) = b

  jointentropy = entropy(ab, 2*nele, nbins)

end function


end module mod_dis_interface
