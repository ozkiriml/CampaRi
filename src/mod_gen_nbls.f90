!--------------------------------------------------------------------------!
! LICENSE INFO:                                                            !
!--------------------------------------------------------------------------!
!    This file is part of CAMPARI.                                         !
!                                                                          !
!    Version 2.0                                                           !
!                                                                          !
!    Copyright (C) 2014, The CAMPARI development team (current and former  !
!                        contributors)                                     !
!                        Andreas Vitalis, Adam Steffen, Rohit Pappu, Hoang !
!                        Tran, Albert Mao, Xiaoling Wang, Jose Pulido,     !
!                        Nicholas Lyle, Nicolas Bloechliger                !
!                                                                          !
!    Website: http://sourceforge.net/projects/campari/                     !
!                                                                          !
!    CAMPARI is free software: you can redistribute it and/or modify       !
!    it under the terms of the GNU General Public License as published by  !
!    the Free Software Foundation, either version 3 of the License, or     !
!    (at your option) any later version.                                   !
!                                                                          !
!    CAMPARI is distributed in the hope that it will be useful,            !
!    but WITHOUT ANY WARRANTY; without even the implied warranty of        !
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         !
!    GNU General Public License for more details.                          !
!                                                                          !
!    You should have received a copy of the GNU General Public License     !
!    along with CAMPARI.  If not, see <http://www.gnu.org/licenses/>.      !
!--------------------------------------------------------------------------!
! AUTHORSHIP INFO:                                                         !
!--------------------------------------------------------------------------!
!                                                                          !
! MAIN AUTHOR:   Andreas Vitalis                                           !
! CONTRIBUTIONS: Nicolas Bloechliger                                       !
! WRAPPER: Davide Garolini                                                 !
!                                                                          !
!--------------------------------------------------------------------------!

module mod_gen_nbls

  real(8) hardcut
  integer maxalcsz ! default variable for the total max allocation size

  ! cnblst,cnc_ids : if cmode = 3(4): snapshot NB-list variables
  type t_cnblst
    integer nbs, alsz ! actual number of neighbors and current allocation size
    real(8), ALLOCATABLE :: dis(:) ! list of distances
    integer, ALLOCATABLE :: idx(:) ! and associated indices
  end type t_cnblst

  type(t_cnblst), ALLOCATABLE :: cnblst(:)

  contains

    subroutine gen_nb(trj)
      use mod_gutenberg
      use mod_clustering
      use mod_variables_gen
      use mod_distance

      implicit none

      real(8), intent(in) :: trj(n_snaps,n_xyz)
      real(8) tmp_d ! temporary variable for radiuses and distances
      integer i, ii, k, kk, j, l, ll, mi, mj
      integer testcnt, testcnt2, overbounds
      real(8) vecti(n_xyz),vectj(n_xyz)
      real(8) maxx !for dist_methods balancing (maximum values)
      ! call spr("DEBUGGING"
      ! call spr("trj_input",trj(1:10,1:10)
      ! call spr("scluster 1", scluster(2)%snaps
      allocate(cnblst(n_snaps))
      cnblst(:)%nbs = 0 ! number of snapshots that are connected to one snap
      cnblst(:)%alsz = 4 ! allocation size
      ! maxalcsz = 4 ! default variable for the total max allocation size
      do i=1,n_snaps
        allocate(cnblst(i)%idx(cnblst(i)%alsz))
        allocate(cnblst(i)%dis(cnblst(i)%alsz))
      end do
      maxx = 0
      ii = 0
      testcnt = 0
      testcnt2 = 0
      overbounds = 0
      call sl()
      call sipr("Distance method: ", dis_method)
      call sl()
      do i=1,nclu ! for each cluster (intra cluster distances)
        ii = ii + 1
        do kk=1,scluster(i)%nmbrs ! for each snapshot in a cluster
          k = scluster(i)%snaps(kk)
          do ll=kk+1,scluster(i)%nmbrs ! for each l != k snapshot
            l = scluster(i)%snaps(ll)
            vecti = trj(k,1:n_xyz)
            vectj = trj(l,1:n_xyz)
            call distance(tmp_d, vecti, vectj)
            if(abs(tmp_d).gt.maxx) maxx = abs(tmp_d)
            ! if(cnblst(k)%nbs.ge.1499) call spr(tmp_d,l,k
            ! compute the distance between all cluster-internal snapshots
            testcnt = testcnt + 1
            if (tmp_d.lt.hardcut) then ! hardcut ext-var CCUTOFF
              testcnt2 = testcnt2 + 1
              ! a_mat(l,k) = tmp_d
              ! a_mat(k,l) = tmp_d
              cnblst(k)%nbs = cnblst(k)%nbs + 1

              if (cnblst(k)%nbs.gt.cnblst(k)%alsz) call cnbl_resz(cnblst(k))
              ! if (cnblst(k)%alsz.gt.maxalcsz) maxalcsz = cnblst(k)%alsz
              cnblst(k)%idx(cnblst(k)%nbs) = l !indexes
              cnblst(k)%dis(cnblst(k)%nbs) = tmp_d !distance val
              cnblst(l)%nbs = cnblst(l)%nbs + 1
              if (cnblst(l)%nbs.gt.cnblst(l)%alsz) call cnbl_resz(cnblst(l))
              cnblst(l)%idx(cnblst(l)%nbs) = k
              cnblst(l)%dis(cnblst(l)%nbs) = tmp_d
            end if
          end do
        end do
        do j=i+1,nclu
          if (scluster(i)%nmbrs.le.scluster(j)%nmbrs) then
            mj = j !mj is major
            mi = i !mi is minor
          else
            mj = i
            mi = j
          end if
          do kk=1,scluster(mi)%nmbrs
            k = scluster(mi)%snaps(kk)
            vecti = trj(k,1:n_xyz)
            call snap_to_cluster_d(tmp_d,scluster(mj),vecti)
            !all the minorcluster snaps vs the mjcluster center
            do ll=1,scluster(mj)%nmbrs
              l = scluster(mj)%snaps(ll)
              vectj = trj(l,1:n_xyz)
              if (scluster(mj)%nmbrs.eq.1) then
                call cluster_to_cluster_d(tmp_d,scluster(mi),scluster(mj))
                testcnt = testcnt + 1
              else
                call distance(tmp_d,vecti,vectj)
                if(abs(tmp_d).gt.maxx) maxx = abs(tmp_d)
                !then you do a complete graph doing euristic nothing
                testcnt = testcnt + 1
              end if
              if (tmp_d.le.hardcut) then
                testcnt2 = testcnt2 + 1
                cnblst(k)%nbs = cnblst(k)%nbs + 1
                if (cnblst(k)%nbs.gt.cnblst(k)%alsz) call cnbl_resz(cnblst(k))
                cnblst(k)%idx(cnblst(k)%nbs) = l !indexes
                cnblst(k)%dis(cnblst(k)%nbs) = tmp_d !distance val
                cnblst(l)%nbs = cnblst(l)%nbs + 1
                if (cnblst(l)%nbs.gt.cnblst(l)%alsz) call cnbl_resz(cnblst(l))
                cnblst(l)%idx(cnblst(l)%nbs) = k
                cnblst(l)%dis(cnblst(l)%nbs) = tmp_d
              end if
            end do
          end do
        end do
      end do

      ! Normalizing
      if(normalize_dis) then
        call srpr("Normalization mode active. Max value that will be &
        & used to normalize (in abs):", maxx)
        call sl()
        do i=1,n_snaps
          cnblst(i)%dis = cnblst(i)%dis / abs(maxx)
        end do
      end if

      ! Adding previous distance if more than one is selected
      call spr('...done')
      call srpr( 'Distances computed (%): ', real((100.0*testcnt)/(0.5*n_snaps*(n_snaps-1)), kind = 8))
      call srpr( 'Distances kept after cutoff (%): ',real((100.0*testcnt2)/(1.0*testcnt), kind = 8))
    end subroutine gen_nb
end module mod_gen_nbls
