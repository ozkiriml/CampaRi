#' @title FSST - Forest of Short Spanning Trees
#' @description Multiple runs of the short spanning tree allow a more precise and robust projection. TODO
#'     
#' @export FSST
#' 


FSST <- function(trj, ann, searchatt = NULL, unispli = NULL, pk_span = NULL, ny = NULL, list_predi_reso = NULL, only_SSTs = FALSE, skip_matching = FALSE, skip_MST_calc = FALSE,
                 sapifaile = "PI_to_delete.dat", base_name = "basename", hyper_method = "jaccard", selection_method = "pbm",
                 camp_exeloc = NULL, leaves.tofold = 0, percs = c(0.9, 0.9), gl_silent = FALSE, 
                 plotting = FALSE, save_plots = TRUE, return_plots = TRUE, dbg_FSST = FALSE){
  .check_pkg_installation("scales")
  # INIT =========================================================================================================
  # camp_exeloc <- "/home/dgarolini/projects/for_campari/apt_campari/campari-3.0.1/bin/x86_64/camp_ncminer_threads" # location of the main CAMPARI executable
  # sapifaile <- "PI_to_delete.dat" # SAPPHIRE file name
  # unispli
  # pk_span
  # ny
  # base_name
  seaatt <- searchatt # number of search attempts to repeat
  n_repetitions <- length(seaatt) # number of repetitions
  sil <- T # internal silent
  
  # some checks
  nullify <- sapply(ann, .isSingleInteger)
  .isSingleChar(sapifaile)
  .isSingleChar(base_name)
  .isSingleChar(hyper_method)
  .isSingleChar(selection_method)
  .checkM(selection_method, c("pbm", "sco"))
  if(!is.null(camp_exeloc)) .isSingleChar(camp_exeloc)
  .isSingleInteger(leaves.tofold, 0, nrow(trj))
  nullify <- sapply(percs, .isSingleNumeric)
  .isSingleLogical(gl_silent)
  .isSingleLogical(plotting)
  .isSingleLogical(save_plots)
  .isSingleLogical(return_plots)
  .isSingleLogical(skip_matching)
  .isSingleLogical(only_SSTs)
  .isSingleLogical(skip_MST_calc)
  
  # direct insertion from second part or not
  if(!is.null(list_predi_reso)){
    if(!gl_silent) cat("Selected direct insertion of the list_predi_reso. Therefore, after some minor checks, this list of vector will be used for the subsequent construction. \n")
    if(only_SSTs) only_SSTs <- FALSE # stupidity proof, yes I can
    stopifnot(all(sapply(list_predi_reso, is.numeric)))
    stopifnot(all(sapply(list_predi_reso, .lt) == .lt(list_predi_reso[[1]])))
    n_repetitions <- .lt(list_predi_reso)
    skip_forest <- T
  }else{
    if(any(sapply(list(searchatt, unispli, pk_span, ny), is.null))) stop("Please provide all the following parameters: searchatt, unispli, pk_span, ny.")
    nullify <- sapply(searchatt, .isSingleInteger)
    nullify <- sapply(unispli, .isSingleInteger)
    .isSingleInteger(pk_span)
    .isSingleInteger(ny)
    skip_forest <- F
  }
  
  # min of ann correction
  uni_ann <- unique(ann)
  if(min(uni_ann) < 1){
    if(!gl_silent) cat('Found negative or 0 values in the annotation. Correcting by sum of the abs of the min +1 per each annotation value. \n')
    ann <- ann + abs(min(uni_ann)) + 1
    uni_ann <- unique(ann)
  }else if(min(uni_ann) > 1){
    if(!gl_silent) cat('Found minimum values > 1 in the annotation. Correcting it automatically. \n')
    ann <- ann - min(uni_ann) + 1
    uni_ann <- unique(ann)
  }
  
  # init outputs
  if(return_plots) return_list_plot <- list()
  return_list <- list()
  
  # MAIN FOREST ------------------------------------------------------------------------
  if(!skip_forest){
    nclpbm <- array(0, n_repetitions) # number of found clusters by means of PBM index
    nbar <- array(0, n_repetitions) # number of found barriers
    # sco <- array(0, n_repetitions) # ARI found # not needed here
    list_predi_reso <- list() # vector of results
    pbmvalues <- NULL
    if(!gl_silent) cat("------------------------------------------------------------------ 1. MAIN CLUSTERING REPETITIONS \n")
    for(i in 1:n_repetitions){
      gc()
      if(!gl_silent) cat("DOING search attempts", seaatt[i], "at point", i, "/", n_repetitions, "\n")
      
      # SST
      SST_campari(trj = trj, sap_file = sapifaile, pi.start = 1, search.attempts = seaatt[i], base_name = base_name,
                  percs = percs, leaves.tofold = leaves.tofold, # no folding and no TBC
                  state.width = round(nrow(trj)/5), multi_threading = T,
                  camp_exe = camp_exeloc, silent = sil, rm_extra_files = T)
      
      # SBR
      out_nSBR <- CampaRi::nSBR(data = sapifaile, comb_met = "euclidean", unif.splits = unispli, hist_exploration = F, 
                                pk_span = pk_span, ny = ny, plot = F, silent = sil, normilize_single_splits = F, dbg_nSBR = F, #n.cluster = 8,
                                which_hist_app = 2, use_density = T, return_plot = F, optimal_merging = T, max_dec_loops = 2,
                                biggest_cl_first = T, over_mic_mean = F, normilize_single_splits = T, normalize_opt_mat = T,
                                dis_clu_met = "mds_kmeans")
      # SCORE
      pl <- score_sapphire(the_sap = sapifaile, manual_barriers = out_nSBR$barriers_sel, plot_pred_true_resume = F, return_plot = F, dbg_score_sapphire = F, add_vlines = F, # , ann = to_ann_plot
                           merging_labels = out_nSBR$df_centers$clu_ind, alternative_method = F, return_predicted_resorted = T, more_hits_first = T, allow_collapse_hits_first = F, silent = sil)
      
      # final assignment
      nclpbm[i] <- length(unique(out_nSBR$df_centers$clu_ind))
      nbar[i] <- length(out_nSBR$barriers_sel)
      pbmvalues <- rbind(pbmvalues, t(out_nSBR$pbm_scores))
      list_predi_reso[[i]] <- pl$predicted_div_resorted
    }
    pbmvalues <- as.data.frame(pbmvalues)
    
    #### OUTPUT: list_predi_reso, nbar, nclpbm
    if(!gl_silent) cat("Best number of clusters ever found across runs is: ", pbmvalues$ncl_vec[which.max(pbmvalues$pbm_scores)[1]], "\n")
    return_list[["list_predi_reso"]] <- list_predi_reso
    return_list[["nbar"]] <- nbar
    return_list[["nclpbm"]] <- nclpbm
    return_list[["pbmvalues"]] <- pbmvalues
    
  }
  
  # direct phase plots
  if(any(plotting, save_plots, return_plots)) {
    
    for(i in 1:n_repetitions){
      if(!gl_silent) cat("Percentage of NAs found:", sum(is.na(list_predi_reso[[i]]))*100.0/length(list_predi_reso[[i]]), "%\n")
      if(anyNA(list_predi_reso[[i]])) list_predi_reso[[i]][is.na(list_predi_reso[[i]])] <- sample(unique(list_predi_reso[[i]])[!is.na(unique(list_predi_reso[[i]]))], size = 1)
    }
    
    # fundamental plot
    the_fin_pl <- print_consensus(list_predi_reso, ann, sile = gl_silent)
    
    # saving the score
    return_list[["sco_direct"]] <- the_fin_pl$score.out
    the_fin_pl <- the_fin_pl$plot
    
    # other plots
    if(!skip_forest){
      distrib_pbm <- ggplot(as.data.frame(pbmvalues), aes(x=ncl_vec, y=pbm_scores)) + theme_classic() +
          geom_vline(aes(xintercept=ncl_vec[which.max(pbm_scores)[1]]), col = "grey") + geom_point(size = 0.6) + 
          geom_point(aes(x=ncl_vec[which.max(pbm_scores)[1]], y=max(pbm_scores)[1]), col = "red", shape=3, size = 2.5) +
          xlab(paste0("Clusters (maximum at ", pbmvalues$ncl_vec[which.max(pbmvalues$pbm_scores)[1]], " clusters)")) + ylab("PBM index") +
        scale_x_continuous(breaks = scales::pretty_breaks())
      ncl_nbar_plot <- ggplot(data.frame("nbar" = nbar, "nclpbm" = nclpbm), aes(x=nclpbm, y=nbar)) + theme_classic() + geom_point(size = 0.6) +
        xlab("Number of clusters") + ylab("Number of barriers") + scale_x_continuous(breaks = scales::pretty_breaks()) + scale_y_continuous(breaks = scales::pretty_breaks())
      the_fin_pl <- cowplot::plot_grid(cowplot::plot_grid(distrib_pbm, ncl_nbar_plot, nrow = 1), the_fin_pl, ncol = 1, rel_heights = c(0.6, 1))
    }
  }else{
    return_list[["sco_direct"]] <- print_consensus(list_predi_reso, ann, sile = gl_silent, no_plots = TRUE)
  }
  
  if(save_plots) ggsave(the_fin_pl, filename = paste0("FINAL_volatility_direct", base_name, ".png"), height = 12.3, width = 18.5)
  if(return_plots) return_list_plot[["the_fin_pl"]] <- the_fin_pl
  if(plotting) print(the_fin_pl)
  
  # if I want only SST forest outputs
  if(only_SSTs){
    if(return_plots) return_list[["all_plots"]] <- return_list_plot
    return(return_list)
  }
  
  # MATCHING ---------------------------------------------------------------------------
  if(!skip_matching){
    # Matching by means of using the real annotation 
    if(!gl_silent) cat("------------------------------------------------------------------ 2. MATCHING \n")
    list_predi_reso_matched <-  list()
    scoreout <- array(0, n_repetitions)
    ncl <- array(0, n_repetitions)
    for(i in 1:n_repetitions){
      if(!gl_silent) cat("ITERATION ", i, "/", n_repetitions, "\n")
      if(!gl_silent) cat("Percentage of NAs found:", sum(is.na(list_predi_reso[[i]]))*100.0/length(list_predi_reso[[i]]), "%\n")
      if(anyNA(list_predi_reso[[i]])) list_predi_reso[[i]][is.na(list_predi_reso[[i]])] <- sample(unique(list_predi_reso[[i]])[!is.na(unique(list_predi_reso[[i]]))], size = 1)
      asdaasc <- CampaRi::score_sapphire(the_sap = list_predi_reso[[i]], ann = ann, plot_pred_true_resume = F, dbg_score_sapphire = F, return_plot = F,
                                         multi_cluster_policy = 'popup', silent = sil, add_vlines = F, super_direct_insertion = T)
      if(!gl_silent) cat("Score found:", asdaasc$score.out, "\n")
      ncl[i] <- length(unique(list_predi_reso[[i]]))
      scoreout[i] <- asdaasc$score.out
      list_predi_reso_matched[[i]] <- asdaasc$predicted_div
    }
    
    ### OUTPUT: list_predi_reso_matched, scoreout
    return_list[["list_predi_reso_matched"]] <- list_predi_reso_matched
    return_list[["scoreout"]] <- scoreout
    
    # plots
    if(any(plotting, save_plots, return_plots)) {
      if(!skip_forest) the_fin_pl_matched <- print_consensus(list_predi_reso_matched, ann, sco = scoreout, nclpbm = ncl, nbar = nbar, sile = gl_silent)
      else the_fin_pl_matched <- print_consensus(list_predi_reso_matched, ann, sco = scoreout, nclpbm = ncl, nbar = NULL, sile = gl_silent)
      # saving the score
      return_list[["sco_matched"]] <- the_fin_pl_matched$score.out
      the_fin_pl_matched <- the_fin_pl_matched$plot
    }else{
      return_list[["sco_matched"]] <- print_consensus(list_predi_reso_matched, ann, sile = gl_silent, no_plots = TRUE)
    }
    if(save_plots) ggsave(the_fin_pl_matched, filename = paste0("FINAL_volatility_matched", base_name, ".png"), height = 12.3, width = 18.5)
    if(return_plots) return_list_plot[["the_fin_pl_matched"]] <- the_fin_pl_matched
    if(plotting) print(the_fin_pl_matched)
    
  }else{
    if(!gl_silent) cat("Matching part skipped (skip_matching = T). \n")
  }
  
  if(!gl_silent) cat("------------------------------------------------------------------ 3.  MCLA and plots \n")
  # MCLA ============================================================================================================
  # Consensus clustering
  l_meta_clu <- list_predi_reso
  pbm_scores_list <- list()
  
  # check for NAs
  if(any(sapply(l_meta_clu, function(x) any(is.na(x))))) {
    if(!gl_silent) cat("Found", sum(sapply(l_meta_clu, function(x) sum(is.na(x)))), "NAs in the label assignments. Fixing automatically with a random sample from unique(). \n")
    for(i in 1:length(l_meta_clu)) 
      l_meta_clu[[i]][is.na(l_meta_clu[[i]])] <- sample(unique(l_meta_clu[[i]])[!is.na(unique(l_meta_clu[[i]]))], size = sum(is.na(l_meta_clu[[i]])), replace = T)
    # sapply(l_meta_clu, function(x) sum(is.na(x)))
  }
  
  # used by plots and further
  nclu_inf <- data.frame("clu" = sapply(l_meta_clu, function(x) length(unique(x))))
  df_unicl <- data.frame("uni_cl" = do.call("c", lapply(l_meta_clu, unique)))
  
  # histogramming the number of clusters
  if(any(plotting, return_plots, save_plots)){
    pphist <- ggplot(data = nclu_inf) + theme_classic() + geom_histogram(aes(x=clu), binwidth = 0.5, fill = "black", col = "black") + xlab("Number of clusters") + 
      scale_x_continuous(breaks = .pretty_breaks(nclu_inf$clu))
    pphist2 <- ggplot(data = df_unicl) + theme_classic() + geom_histogram(aes(x=uni_cl), binwidth = 0.5, fill = "black", col = "black") + xlab("Found labels") +
      scale_x_continuous(breaks = .pretty_breaks(df_unicl$uni_cl))
    cw_pphist <- cowplot::plot_grid(pphist, pphist2, ncol = 1)
  }
  
  # loop for creating the hypergraph
  total_he <- NULL
  if(!gl_silent) cat("Creating hypergraph representation... ")
  for(i in 1:length(l_meta_clu)){
    hyperedge1 <- lapply(unique(l_meta_clu[[i]]), function(x) as.numeric(l_meta_clu[[i]] == x))
    hyperedge1 <- do.call("cbind", hyperedge1)
    total_he <- cbind(total_he, hyperedge1)
  }
  if(!gl_silent) cat("done. \n")
  
  # check some comparison / calculating jaccard distance
  if(!gl_silent) cat("Using ", hyper_method, "distance/similarity to generate the hypergraph distance matrix... ")
  if(hyper_method == "jaccard"){
    hypermat_veg <- vegan::vegdist(t(total_he), method = hyper_method)
  }else if(hyper_method == "kullback-leibler"){
    total_he2 <- apply(total_he, 2, function(x) x/sum(x)) # needs normalization
    hypermat_veg <- philentropy::distance(t(total_he2), method = hyper_method)
  }else{
    hypermat_veg <- philentropy::distance(t(total_he2), method = hyper_method)
  }
  if(!gl_silent) cat("done. \n")
  
  # clustering with mds and kmeans using the ncl found with the max PBM index
  if(!gl_silent) cat("Using multidimensional scaling and", selection_method," along with kmeans++ to select the right number of clusters in the hypergraph distance matrix... ")
  sdm <- smacof::mds(as.matrix(hypermat_veg), ndim = 2) # smacof mds expansion
  scores_fin <- lapply(unique(nclu_inf$clu), function(x) {
    clumeans <- ClusterR::KMeans_rcpp(data = sdm$conf, clusters = x, initializer = "kmeans++", num_init = 20)$clusters
    pbm <- clusterCrit::intCriteria(traj = sdm$conf, part = as.integer(clumeans), crit = "PBM")$pbm
    final_rec_he <- NULL
    for(i in unique(clumeans)) {
      if(sum(clumeans == i) == 1) final_rec_he <- cbind(final_rec_he, sapply(total_he[, clumeans == i], mean))
      else final_rec_he <- cbind(final_rec_he, apply(total_he[, clumeans == i], 1, mean))
    }
    superfin_clu <- apply(final_rec_he, 1, which.max)
    out_put <- CampaRi::score_sapphire(the_sap = superfin_clu, ann = ann, plot_pred_true_resume = F, dbg_score_sapphire = F, 
                                       return_plot = F, plot_result = F, return_predicted_resorted = F,
                                       multi_cluster_policy = 'popup', silent = T, add_vlines = F, super_direct_insertion = T)
    return(data.frame("pbm" = pbm, "sco" = out_put$score.out))
  })
  scores_fin <- cbind("ncl" = unique(nclu_inf$clu), do.call("rbind", scores_fin))
  best_pbm <- scores_fin$ncl[which.max(scores_fin$pbm)]
  best_sco <- scores_fin$ncl[which.max(scores_fin$sco)]
  
  if(selection_method == "pbm"){
    best_ncl <- best_pbm
  }else if(selection_method == "sco") {
    best_ncl <- best_sco
  }
  clumeans <- ClusterR::KMeans_rcpp(data = sdm$conf, clusters = best_ncl, initializer = "kmeans++", num_init = 20)$clusters
  if(!gl_silent) cat("done. \n")
  
  # final recomposition of the hyperdeges following the metaclustering
  final_rec_he <- NULL
  for(i in unique(clumeans)) {
    if(sum(clumeans == i) == 1) final_rec_he <- cbind(final_rec_he, sapply(total_he[, clumeans == i], mean))
    else final_rec_he <- cbind(final_rec_he, apply(total_he[, clumeans == i], 1, mean))
  }
  
  # normalizing and hardening
  final_rec_he_norm <- apply(final_rec_he, 1, function(x) x/sum(x))
  superfin_clu <- apply(final_rec_he, 1, which.max)
  
  # found correspondences
  asdaasc <- CampaRi::score_sapphire(the_sap = superfin_clu, ann = ann, plot_pred_true_resume = F, dbg_score_sapphire = F, 
                                     return_plot = any(return_plots, save_plots), plot_result = any(return_plots, save_plots), return_predicted_resorted = T,
                                     multi_cluster_policy = 'popup', silent = gl_silent, add_vlines = F, super_direct_insertion = T)
  
  
  # trying to find a volatility measure
  volatility <- apply(final_rec_he, 1, function(x) sum(x > 0.0000001))
  
  if(dbg_FSST) browser()
  
  # FINAL PLOTS
  if(any(plotting, return_plots, save_plots)){
    # plots of the mds+kmeans++
    # correcting the assignment to the matched one from score_sapphire
    if(.ltuni(clumeans) == .ltuni(asdaasc$predicted_div)){
      tmp_1 <- as.factor(clumeans)
      levels(tmp_1) <- unique(asdaasc$predicted_div)
      clumeans <- .de_factor(tmp_1)
      col_hypergraph_clu <- "Set2"
    } else {
      if(!gl_silent) cat("The clustering of the hypergraph distance matrix found some cluster which was never selected in the hardening step, i.e. was never the highest probable.\n")
      if(!gl_silent) cat("Therefore, it has been removed. In this case the color setting of the MCLA step will be set with a different palette in comparison to the inferred/predicted one.\n")
      if(!gl_silent) cat("The length(unique()) of the MCLA clustering and hardening are respectively:", .lt(unique(clumeans)), .lt(unique(asdaasc$predicted_div)),"\n")
      col_hypergraph_clu <- "Greens"
      
    }
    # NB: it can happen that in the process of hardening some cluster is never chosen from. In this case we would end up with less clusters
    #     than found in the pbm scoring of the hypergraph distance matrix. We solve the problem by spawning new clusters (?)
    
    # plot the hypergraph distance matrix
    tpm <- tile_plot_mat(as.matrix(hypermat_veg), annot = clumeans, col_ann = col_hypergraph_clu, col = "Blues", return_plot = T, plot = F)
    
    # plotting the mds and clustering
    df_mds_cl <- data.frame("D1" = sdm$conf[,1], "D2" = sdm$conf[,2], "Cluster" = .factorOrnot(clumeans))
    mds_clu_pl <- ggplot(data = df_mds_cl) + theme_classic() + geom_point(aes(x=D1, y=D2, col=Cluster)) + .palettit(.ltuni(clumeans) < 8, col_hypergraph_clu)
    
    # plotting the PBM indexes
    pbm_ind_pl <- ggplot(data = scores_fin) + theme_classic() + geom_point(aes(x=ncl, y=pbm), col = "black") + geom_point(aes(x=ncl, y=sco), col = "grey") +
      geom_point(x=best_pbm, y=scores_fin$pbm[unique(nclu_inf$clu) == best_pbm], col = "Red", shape = 1) + 
      geom_point(x=best_sco, y=scores_fin$sco[unique(nclu_inf$clu) == best_sco], col = "Red", shape = 1) + 
      scale_x_continuous(breaks =.pretty_breaks(nclu_inf$clu)) + xlab("Number cluster") + ylab("PBM")
    
    # plotting pbm vs scores
    scores_fin$ncl <- .factorOrnot(scores_fin$ncl)
    pbm_sco_pl <- ggplot(data = scores_fin, aes(x=sco, y=pbm, col = ncl), size = 1.2) + theme_classic() + geom_point() +
      xlab("ARI") + ylab("PBM") +theme(legend.title = element_blank()) + .palettit(.ltuni(scores_fin$ncl) < 8, "Blues")
    
    # plot_grid
    optimal_merging_pl <- cowplot::plot_grid(cowplot::plot_grid(cowplot::plot_grid(pbm_ind_pl, pbm_sco_pl, ncol = 1), mds_clu_pl, ncol = 1), tpm, nrow = 1, rel_widths = c(0.55, 1))
    
    # volatility data.frames
    df_volat <- data.frame("Volatility" = volatility, "True" = as.factor(ann))
    df_volat2 <- data.frame("Volatility" = volatility, "Consensus" = as.factor(asdaasc$predicted_div)) # using label correctly associated
    
    # volatiliry histogram
    vol_plot <- ggplot(df_volat) + geom_histogram(aes(x = Volatility, fill = True), binwidth = 1, col = "black") + 
      theme_minimal() + scale_fill_brewer(type = "qual", palette = "Set1") + theme(panel.grid = element_blank()) + scale_x_continuous(breaks = .pretty_breaks(volatility))
    vol_plot2 <- ggplot(df_volat2) + geom_histogram(aes(x = Volatility, fill = Consensus), binwidth = 1, col = "black") + 
      theme_minimal() + scale_fill_brewer(type = "qual", palette = "Set2") + theme(panel.grid = element_blank()) + scale_x_continuous(breaks = .pretty_breaks(volatility))
    
    # finding the final SST/MST
    if(skip_MST_calc){
      if(is.null(seaatt)) seaatt <- nrow(trj) # small hack
      if(!gl_silent) cat("Using", min(nrow(trj), ceiling(length(seaatt) * max(seaatt))), "search attempts to find really precise tree...\n")
      SST_campari(trj = trj, sap_file = sapifaile, pi.start = 1, search.attempts = min(nrow(trj), ceiling(length(seaatt) * max(seaatt))), base_name = base_name,
                  percs = percs, leaves.tofold = leaves.tofold, # no folding and no TBC
                  state.width = round(nrow(trj)/5), multi_threading = T,
                  camp_exe = camp_exeloc, silent = sil, rm_extra_files = T)
      
    }
    # Sapphire plot according to volatility measure
    spconse_vol <- sapphire_plot_simply(sap_file = sapifaile, sap_table = NULL, ann_trace = volatility, local_cut = TRUE, timeline = TRUE, ann_palette = "Blues",
                                        sub_sampling_factor = 1, return_plot = T, silent = sil, reorder_annotation = TRUE, annotate_snap_dist = TRUE)
    
    # top standing probabilities of belonging to a certain cluster or another (SOFT clustering)
    saptab <- data.table::fread(sapifaile, showProgress=FALSE, data.table = F)
    yminvec <- NULL; for(i in 1:nrow(final_rec_he_norm)) yminvec <- c(yminvec, rep(i, nrow(saptab)))
    tovol_df <- data.frame("xmin" = rep(1:nrow(saptab), nrow(final_rec_he_norm)), "ymin" = yminvec - 1, "ymax" = yminvec, "filling" = c(t(final_rec_he_norm[,saptab$V3])))
    tovol_df_ann <- data.frame("xmin" = 1:nrow(saptab), "ymin" = rep(0, nrow(saptab)), "filling_ann" = as.factor(ann[saptab$V3]))
    tovol_df_pred <- data.frame("xmin" = 1:nrow(saptab), "ymin" = rep(0, nrow(saptab)), "filling_ann" = as.factor(asdaasc$predicted_div[saptab$V3]))
    thevolsap_pl <- ggplot(data = tovol_df) + geom_rect(mapping = aes(xmin = xmin, ymin = ymin, xmax = xmin + 1, ymax = ymax, fill = filling)) + scale_fill_distiller(palette = "Blues") +
      theme_void() + theme(legend.position = "none", plot.margin = unit(c(0,0,0,0), "cm")) 
    thevolsap_pl_ann <- ggplot(data = tovol_df_ann) + geom_rect(mapping = aes(xmin = xmin, ymin = ymin, xmax = xmin + 1, ymax = ymin + 1, fill = filling_ann)) + scale_fill_brewer(palette = "Set1")+
      theme_void() + theme(legend.position = "none", plot.margin = unit(c(0,0,0,0), "cm"))
    thevolsap_pl_pred <- ggplot(data = tovol_df_pred) + geom_rect(mapping = aes(xmin = xmin, ymin = ymin, xmax = xmin + 1, ymax = ymin + 1, fill = filling_ann)) + scale_fill_brewer(palette = "Set2")+
      theme_void() + theme(legend.position = "none", plot.margin = unit(c(0,0,0,0), "cm"))
    
    # merge of the volatility
    spconse_vol_fin <- cowplot::plot_grid(thevolsap_pl_ann, thevolsap_pl_pred, thevolsap_pl, spconse_vol + theme(legend.position = "none"), align = "v", ncol = 1, rel_heights = c(0.1, 0.1, 0.1, 0.9))
    
    # final merge for MCLA
    cw_fin <- cowplot::plot_grid(cw_pphist, optimal_merging_pl, asdaasc$plot, spconse_vol_fin, vol_plot, vol_plot2)
    if(save_plots) ggsave(cw_fin, filename = paste0("FINAL_volatility_metaclu_consensus", base_name,"_met", hyper_method, ".png"), width = 23, height = 12)
    if(return_plots) return_list_plot[["cw_fin"]] <- cw_fin
    if(plotting) print(cw_fin)
  }
  
  ### OUTPUT: hypermat_veg, final_rec_he_norm, superfin_clu, volatility, plots
  return_list[["final_score"]] <- asdaasc$score.out
  return_list[["hypermat_veg"]] <- hypermat_veg
  return_list[["final_rec_he_norm"]] <- final_rec_he_norm
  return_list[["superfin_clu"]] <- superfin_clu
  return_list[["volatility"]] <- volatility
  if(return_plots) return_list[["all_plots"]] <- return_list_plot
  invisible(return_list)
}

#
#
#

# Print consensus ----------------------------------------------------------------------------------- 
# 
#' @title Print consensus clustering
#' @description This function allows you to print and create a figure with the standard volatility, i.e., with all the clustering already defined correctly in a long list (to_vol). The 
#' @param to_vol List of label vectors, which are already defined "correctly".
#' @param to_ann_plot True annotation must be provided because this functions is a producing a final evaluation of the consensus results (using \link{\code{score_sapphire}}).
#' @param sco This is a vector of scores, it is referring to the param to_vol, meaning that each of this score has been generated from one of the predicted vectors.
#' @param nclpbm Similarly, this is a vector of found number of clusters by means of PBM index.
#' @param nbar Vector of numbers of barriers found in the nSBR run.
#' @param win Vector of windows used. Can be left NULL.
#' @export print_consensus

print_consensus <- function(to_vol, to_ann_plot, sco = NULL, nclpbm = NULL, nbar = NULL, win = NULL, sile = FALSE, no_plots = FALSE){
  if(no_plots){
    consensus_v <- apply(do.call("cbind", to_vol), 1, function(x) as.numeric(names(sort(table(x), decreasing = T))[1]))
    asdaasc <- CampaRi::score_sapphire(the_sap = consensus_v, ann = to_ann_plot, plot_pred_true_resume = F, dbg_score_sapphire = F, return_plot = F, plot_result = F,
                                       multi_cluster_policy = 'popup', silent = sile, add_vlines = F, super_direct_insertion = T)
    return(asdaasc$score.out)
  }
  
  if(!is.null(sco)){
    # main data.frame
    tot_mat <- data.frame("ARI" = sco, "PBM_ncl" = nclpbm)[1:length(to_vol),]
    if(!is.null(win)) tot_mat <- cbind(tot_mat, "win" = win)[1:length(to_vol),]
    if(!is.null(nbar)) tot_mat <- cbind(tot_mat, "nbar" = nbar)[1:length(to_vol),]
    
    # number of clusters
    if(!is.null(win)) gg_pbm_ncl <- ggplot(tot_mat, aes(y = ARI, x = PBM_ncl, col = win)) + geom_point() + theme_classic() + 
        xlab("Number of clusters") + ylab("ARI") + scale_x_continuous(breaks = scales::pretty_breaks())
    else gg_pbm_ncl <- ggplot(tot_mat, aes(y = ARI, x = PBM_ncl)) + geom_point() + theme_classic() + 
        xlab("Number of clusters") + ylab("ARI") + scale_x_continuous(breaks = scales::pretty_breaks())
    
    # dependence on the window
    if(!is.null(win)) gg_pbm_win <- ggplot(tot_mat, aes(y = ARI, x = win)) + geom_point() + theme_classic() + theme(legend.position = "none") + xlab("Window size") + ylab("ARI")
    
    # number of barriers
    if(!is.null(nbar)) {
      if(!is.null(win)) gg_pbm_nbar <- ggplot(tot_mat, aes(y = ARI, x = nbar, col = win)) + geom_point() + theme_classic() + theme(legend.position = "none") + xlab("Number of barriers") + ylab("ARI") +
          scale_x_continuous(breaks = scales::pretty_breaks())
      else gg_pbm_nbar <- ggplot(tot_mat, aes(y = ARI, x = nbar)) + geom_point() + theme_classic() + theme(legend.position = "none")  + xlab("Number of barriers") + ylab("ARI") + 
          scale_x_continuous(breaks = scales::pretty_breaks())
    }
    gg_hist_sco <- ggplot(tot_mat) + geom_histogram(mapping = aes(x=ARI), fill = "Grey15", col = "Black", bins = 25) + theme_classic()
    
    pl_list <- list(gg_pbm_ncl, gg_hist_sco)
    if(!is.null(win)) pl_list[[length(pl_list) + 1]] <- gg_pbm_win
    if(!is.null(nbar)) pl_list[[length(pl_list) + 1]] <- gg_pbm_nbar
    first_layer <- cowplot::plot_grid(plotlist = pl_list, nrow = 1)
    if(!sile) print(head(tot_mat[order(tot_mat$ARI, decreasing = T),]))
  }
  if(min(to_ann_plot) < 1) to_ann_plot <- to_ann_plot + abs(min(to_ann_plot)) + 1
  consensus_v <- unlist(apply(do.call("cbind", to_vol), 1, function(x) as.numeric(names(sort(table(x, exclude = NA), decreasing = T))[1])), use.names = F)
  volatility <- apply(do.call("cbind", to_vol), 1, function(x) length(sort(table(x))))
  
  df_volat <- data.frame("Volatility" = volatility, "True" = as.factor(to_ann_plot))
  df_volat2 <- data.frame("Volatility" = volatility, "Consensus" = as.factor(consensus_v))
  
  vol_plot <- ggplot(df_volat) + geom_histogram(aes(x = Volatility, fill = True), binwidth = 1, col = "black") + 
    theme_minimal() + scale_fill_brewer(type = "qual", palette = "Set1") + theme(panel.grid = element_blank()) + scale_x_continuous(breaks = .pretty_breaks(volatility))
  vol_plot2 <- ggplot(df_volat2) + geom_histogram(aes(x = Volatility, fill = Consensus), binwidth = 1, col = "black") + 
    theme_minimal() + scale_fill_brewer(type = "qual", palette = "Set2") + theme(panel.grid = element_blank()) + scale_x_continuous(breaks = .pretty_breaks(volatility))
  
  asdaasc <- CampaRi::score_sapphire(the_sap = consensus_v, ann = to_ann_plot, plot_pred_true_resume = F, dbg_score_sapphire = F, return_plot = T, plot_result = T,
                                     multi_cluster_policy = 'popup', silent = sile, add_vlines = F, super_direct_insertion = T)
  
  vol_subplot <- cowplot::plot_grid(vol_plot, vol_plot2, ncol = 1)
  
  if(!is.null(sco)) {
    second_layer <- cowplot::plot_grid(asdaasc$plot, vol_subplot, rel_widths = c(1, 0.75))
    fingg1 <- cowplot::plot_grid(first_layer, second_layer, ncol = 1, rel_heights = c(0.3, 1))
  } else {
    fingg1 <- cowplot::plot_grid(asdaasc$plot, vol_subplot, rel_widths = c(1, 0.75))
  }
  invisible(list("plot" = fingg1, "score.out" = asdaasc$score.out))
}
