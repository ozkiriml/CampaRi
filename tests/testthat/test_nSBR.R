context('nSBR')

test_that('new trials for SBR', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  stdd <- 3; n_dim <- 10; n_snap <- 3000; n_tot <- n_dim*n_snap/3; if(!silent) print(n_tot)
  ann <- c(rep(1, n_snap/6), rep(2, n_snap/3), rep(1, n_snap/6), rep(3, n_snap/3))
  file.pi <- system.file("extdata", "REPIX_000000000021.dat", package = "CampaRi")
  if(plt_stff) sapphire_plot(sap_file = file.pi, ann_trace = ann, only_timeline=F, timeline = T)
  data.pi <- data.table::fread(file = file.pi, data.table = F)
  nr <- floor(nrow(data.pi)/2); tot_lpi <- nrow(data.pi)
  data.pi <- rbind(data.pi[(nr+1):tot_lpi,], data.pi[1:nr,])
  data.pi[,1] <- 1:tot_lpi
  
  # BASINS OPTIMIZATION ======================================================================================
  nbin <- round(sqrt(n_snap*10)); if(!silent) print(nbin)
  ev_it(optimal_bas <- CampaRi::nSBR(data = file.pi, ny = 30, n.cluster = 4, plot = T, silent = silent, dbg_nSBR = F), s = silent, err = F)
  
  
  # classic test ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, 
                                   comb_met = c('MIC_kin', 'MIC', 'MAS', 'MEV', 'MCN', 'MICR2', 'MI', 'kin', 'diff', 'convDiff', 'conv'),
                                   unif.splits = seq(5, 100, 8),  
                                   pk_span = 500, ny = 50, plot = T, 
                                   silent = silent, dbg_nSBR = F, return_plot = F), s = silent, err = T)
  # TODO: extend
  
  
  # random test ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, 
                                   comb_met = c('MIC_kin'),
                                   unif.splits = seq(5, 100, 8),  
                                   pk_span = 500, ny = 50, plot = T, random_picks = 100, 
                                   silent = silent, dbg_nSBR = F, return_plot = F), s = silent, err = T)

  # shuffles test ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, shuffles = T, 
                                   comb_met = "MIC",
                                   unif.splits = seq(5, 100, 8),  
                                   pk_span = 500, ny = 20, plot = T, random_picks = 100, ann = ann,
                                   silent = silent, dbg_score_sapphire = F, return_plot = F), silent)
  ev_it(null <- capture_output(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, shuffles = T, 
                                   comb_met = c('MIC'),
                                   unif.splits = seq(5, 100, 8),  
                                   pk_span = 500, ny = 20, plot = T, random_picks = 100, ann = ann,
                                   silent = F, dbg_score_sapphire = F, return_plot = F)), s = silent)
  
  # force_correct_ncl test ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 15, force_correct_ncl = T, ny = 10, pk_span = 500,
                                   unif.splits = seq(5, 100, 8), silent = silent, plot = T), s = silent, err = F) # too many clusters for only 7 loops
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = NULL, force_correct_ncl = T, ny = 10, pk_span = 500,
                                   unif.splits = seq(5, 100, 8), silent = silent, plot = T), s = silent, err = T) # collision with the force number of clusters
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 8, force_correct_ncl = T, ny = 100, pk_span = 350,
                                   unif.splits = seq(5, 100, 8), silent = silent, plot = T), s = silent)
  ev_it(null <- capture_output(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 8, force_correct_ncl = T, ny = 100, pk_span = 350,
                                      unif.splits = seq(5, 100, 8), silent = silent, plot = T)), s = silent)
  
  # plotting histograms ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MEV'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T,
                                   silent = silent, dbg_nSBR = F, hist_exploration = T), s = silent, err = T) 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T, 
                                   silent = silent, dbg_nSBR = F, return_plot = T, hist_exploration = T), s = silent)  
  
  # in parallel! ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MI'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T,
                                   silent = silent, dbg_nSBR = F, n_cores = 2, return_plot = T), s = silent, err = T) # error because it works only with MIC
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MI'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T,
                                   silent = silent, dbg_nSBR = F, n_cores = 160, return_plot = T), s = silent, err = T) # error because too many cores
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MI'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T,
                                   silent = silent, dbg_nSBR = F, n_cores = 2, return_plot = T, hist_exploration = T), s = silent, err = T) # error because hist exploration is not allowed with multicore
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 200, ny = 30, plot = T, 
                                   silent = silent, dbg_nSBR = F, n_cores = 2, return_plot = T), s = silent)
  
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 200, ny = 30, plot = T, n_cores_opt = 1,
                                   silent = silent, dbg_nSBR = F, n_cores = 2, return_plot = T), s = silent)
  
  
  # testing automatic span increaser ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 5, comb_met = c('MIC'), max_dec_loops = 1, force_correct_ncl = T,
                                     unif.splits = seq(5, 100, 8), pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent), s = silent, err = T) # number of clusters not possible to find
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 2, comb_met = c('MIC'), max_dec_loops = 0,
                                     unif.splits = seq(5, 100, 8), pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent), s = silent, err = T) # max_dec_loops too low
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 2, comb_met = c('MIC'), max_dec_loops = "mele",
                                     unif.splits = seq(5, 100, 8), pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent), s = silent, err = T) # max_dec_loops not a single int
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 2, comb_met = c('MIC'), unif.splits = seq(5, 100, 8),
                                   pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent, max_dec_loops = 50), s = silent)
  
  # new merging system ------------------------------------------------------- 
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 80, 8), pk_span = 200, ny = 100, plot = T, silent = silent, force_correct_ncl = T, optimal_merging = T), s = silent, err = T)
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 80, 8), pk_span = 200, ny = 100, plot = T, silent = silent, return_plot = T, optimal_merging = T, dbg_nSBR = F), s = silent)
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 80, 8), pk_span = 200, ny = 100, plot = T, silent = silent, return_plot = T, optimal_merging = T, n_cores = 2), s = silent)
  ev_it(a1 <- CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 80, 8), hist_exploration = T, pk_span = 200, ny = 100, plot = T, silent = silent, normilize_single_splits = F, return_plot = T, optimal_merging = T), s = silent)
  
  
  # new hist method ---------------------------------------------------------------
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = F), s = silent) # new histsss
  # new hist # optimal merging using counts and single IMIC unif normalization
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = T, dbg_nSBR = F,
                      which_hist_app = 2, use_density = F, return_plot = T, optimal_merging = T), s = silent) 
  # old 2d hist
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 1, use_density = T, return_plot = T, optimal_merging = F), s = silent) 
  # old 2d hist # optimal merging using counts and single IMIC unif normalization
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = T, dbg_nSBR = F,
                      which_hist_app = 1, use_density = F, return_plot = T, optimal_merging = T), s = silent) 
  
  
  # cluster assignment optimization -----------------------------------------------------------------------------------------------------------------------------------------
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T), s = silent) # standard use of the thresh
  ev_it(CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = F, first_cl_labels = F), s = silent)   # first_cl_labels FALSE
  ev_it(CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = F,
                      first_cl_labels = F, over_mic_mean = F), s = silent) # first_cl_labels FALSE and the use of the barrier average TRUE
  ev_it(CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = F
                      , first_cl_labels = F, over_mic_mean = F, max_n.cl = T, n.cluster = 2), s = silent) # first_cl_labels FALSE and the use of the barrier average TRUE
  ev_it(CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, 
                      pk_span = 200, ny = 50, plot = T, silent = silent, normilize_single_splits = F, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = F, 
                      over_mic_mean = T, max_n.cl = T), s = silent, err = T) # max_n.cl = T
  # nr <- floor(nrow(data.pi)/3); tot_lpi <- nrow(data.pi)
  # data.pi <- rbind(data.pi[(nr+1):tot_lpi,], data.pi[1:nr,])
  # data.pi[,1] <- 1:tot_lpi
  # ev_it(CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F,
  #                     pk_span = 200, ny = 100, plot = T, silent = F, normilize_single_splits = F, dbg_nSBR = F,
  #                     which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, consec_merging = T,
  #                     opt_m_thresh = 0.8, cons_m_thresh = 0.6, first_cl_labels = T, over_mic_mean = T), s = silent)
  # PBM -----------------------------------------------------------------------------------------------------------------------------------------
  # to show: the pbm index plot
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('euclidean'), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T), s = silent) # standard use of the thresh
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('euclidean'), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, PBM_ncl = 10, meanMinusSdest_cl_first = T), s = silent) # fixing the number of PBM_ncl
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('euclidean'), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, PBM_ncl = 10, meanest_cl_first = T), s = silent) # fixing the number of PBM_ncl
  ev_it(CampaRi::nSBR(data = file.pi, comb_met = c('euclidean'), unif.splits = seq(5, 120, 3), pk_span = 200, ny = 150, plot = T, dbg_nSBR = F,
                      which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, PBM_ncl = 4, biggest_cl_first = T), s = silent) # fixing the number of PBM_ncl
  
  
  # F STUFF -------------------------------------------------------------------------------------
  if(F){
    # F neural anal ----------------------------------------------------------------------------------
    # look for same numbers as before, e.g. 3000/50 = 100000/x
    # ny_it = round(100000*50/3000)
    ny_it = 40
    # seq_it = round(100000*seq(5, 120, 8)/3000)
    seq_it = seq(5,200,5)
    
    f1 <- "/disk2a/dgarolini/neuro/simulated_data/code_complete/analysis/PI_output_trjana_N10_npat2_MVtot__40splits_100s_2500meanLsplits_pistart6_searchatt8000_leaves15_locut50000_PCA15.dat"
    f1c <- "/disk2a/dgarolini/neuro/simulated_data/code_complete/simulations/sim_N10_npat2_clu_vec__40splits_100s_2500meanLsplits.npy"
    cv1 <- RcppCNPy::npyLoad(f1c, type = 'integer')
    f2 <- "/disk2a/dgarolini/neuro/simulated_data/code_complete_2/analysis/PI_output_trjana_N70_npat4_Ndis14_MVtot__39splits_100s_2564meanLsplits_pistart2_searchatt8000_leaves15_locut25000_PCA15.dat"
    f2c <- "/disk2a/dgarolini/neuro/simulated_data/code_complete_2/simulations/sim_N70_npat4_Ndis14_clu_vec__39splits_100s_2564meanLsplits.npy"
    cv2 <- RcppCNPy::npyLoad(f2c, type = 'integer')
    
    cv1 <- sample(c(1:15), 100000, replace = T)
    # most challenging two states 
    a1 <- CampaRi::nSBR(data = f1,
                        comb_met = c('MIC'), unif.splits = seq_it, pk_span = 30000, ny = ny_it, plot = T, silent = F, return_plot = T, optimal_merging = T, n_cores = 5, 
                        normilize_single_splits = F, consec_merging = T, opt_m_thresh = 0.6, cons_m_thresh = 0.6, first_cl_labels = T, over_mic_mean = T)
    asc <- CampaRi::score_sapphire(the_sap = f1, ann = cv1, plot_pred_true_resume = T, dbg_score_sapphire = F, multi_cluster_policy = 'popup',
                                   manual_barriers = a1$barriers_sel, silent = silent, merging_labels = a1$df_centers$clu_ind)    
    # most challenging split states
    a2 <- CampaRi::nSBR(data = f2,
                        comb_met = c('MIC'), unif.splits = seq_it, pk_span = 10000, ny = ny_it, plot = T, silent = F, return_plot = T, optimal_merging = T, n_cores = 5, dbg_nSBR = F,
                        normilize_single_splits = F, consec_merging = T, opt_m_thresh = 0.6, cons_m_thresh = 0.6, first_cl_labels = T, over_mic_mean = T)
    # sapphire_plot(f2, ann_trace = cv2, timeline = T)
    asc <- CampaRi::score_sapphire(the_sap = f2, ann = cv2, plot_pred_true_resume = T, dbg_score_sapphire = F, multi_cluster_policy = 'popup',
                                   manual_barriers = a2$barriers_sel, silent = silent, merging_labels = a2$df_centers$clu_ind)
  }
  
  # F speedup tests --------------------------------------------------
  if(F){
    tic()
    a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), random_picks = 10, pk_span = 500, ny = 1000, plot = T, ann = ann,
                        silent = silent, dbg_nSBR = F, n_cores = 6, return_plot = T)
    toc()
    tic()
    a1 <- CampaRi::nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), random_picks = 10, pk_span = 500, ny = 1000, plot = T, ann = ann,
                        silent = silent, dbg_nSBR = F, n_cores = NULL, return_plot = T)
    toc()
  } 
  # F optim speed --------------------------------------------------
  if(F){
    library(microbenchmark); library(ggplot2)
    a <- microbenchmark(
      "with_cores" = CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F, n_cores = 5,
                                pk_span = 20, ny = 20, plot = F, silent = T, normilize_single_splits = T, dbg_nSBR = F,
                                which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, consec_merging = T, 
                                opt_m_thresh = 0.9, cons_m_thresh = 0.9, first_cl_labels = F, over_mic_mean = F),
      "without" = CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F,
                                  pk_span = 20, ny = 20, plot = F, silent = T, normilize_single_splits = T, dbg_nSBR = F,
                                  which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, consec_merging = T, 
                                  opt_m_thresh = 0.9, cons_m_thresh = 0.9, first_cl_labels = F, over_mic_mean = F),
      "hybrid" = CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F,  n_cores = 5, n_cores_opt = NULL,
                                  pk_span = 20, ny = 20, plot = F, silent = T, normilize_single_splits = T, dbg_nSBR = F,
                                  which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, consec_merging = T, 
                                  opt_m_thresh = 0.9, cons_m_thresh = 0.9, first_cl_labels = F, over_mic_mean = F),
      times = 5
    )    
    autoplot(a)
    profvis::profvis( CampaRi::nSBR(data = data.pi, comb_met = c('MIC'), unif.splits = seq(5, 120, 8), hist_exploration = F,  n_cores = 5, n_cores_opt = NULL, # if you put 0 the bottleneck is makeCluster
                                    pk_span = 20, ny = 20, plot = F, silent = T, normilize_single_splits = T, dbg_nSBR = F,
                                    which_hist_app = 2, use_density = T, return_plot = T, optimal_merging = T, consec_merging = T, 
                                    opt_m_thresh = 0.9, cons_m_thresh = 0.9, first_cl_labels = F, over_mic_mean = F))
    
  }
  
  # F hist speedup ---------------------------------------------------
  if(F){
    x <- as.matrix(data.table::fread(file = file.pi, data.table = F))
    x <- x[,c(1,3)] # PI and time
    
    ny <- 100; lpi <- 3000
    nsplits <- 10
    hist_brks <- seq(1, lpi, length.out = ny + 1)
    brks2 <- floor(seq(1, lpi, length.out = nsplits))[-c(1,nsplits)]
    plot(x[1:brks2[1], 2])
    # plot(x[brks2[1]:lpi, 2])
    plot(x[brks2[1]:brks2[2], 2])
    
    ua <- .switcher_hist(1, x, brks2, hist_brks, lpi = lpi, density = F)
    ub <- .switcher_hist(2, x, brks2, hist_brks, lpi = lpi, density = F)
    nrow(ua); nrow(ub)
    plot(ua[,1])
    points(ub[,1], col = "red")
    plot(ua[,2])
    points(ub[,2], col = "red")
    aa <- gplots::hist2d(progind[,1:2], show = T, ncol = 2, nbins = c(ny, ny))
    az <- hist(progind[1500:3000,2], plot = T, breaks = hist_brks, include.lowest = T, right = F)
    sum(aa$y.breaks == az$breaks)
    sum(az$mids == aa$y)

    
    library(microbenchmark); library(ggplot2)
    a <- microbenchmark(
      "2d hist" = .switcher_hist(1, x, brks2, hist_brks, lpi = lpi),
      "fast hist" = .switcher_hist(2, x, brks2, hist_brks, lpi = lpi),
      times = 300
    )    
    a <- microbenchmark(
      "aa" = gplots::hist2d(progind[,1:2], show = F, ncol = 2, nbins = c(ny, ny)),
      "az" = hist(progind[,2], plot = F, breaks = hist_brks, include.lowest = T, right = F),
      "au" = cut(progind[,2], hist_brks, include.lowest = T),
      times = 100
    )
    autoplot(a)
    
    
    .switcher_hist <- function(which_one, pi, barr, hist_brks, lpi = NULL, density = TRUE){
      if(which_one == 1){
        return(.dens_histCounts(progind = pi, barr = barr, ny = .lt(hist_brks) - 1, lpi = lpi, density = density))
      }else if(which_one == 2){
        return(.horiz_hist(progind = pi, barr = barr, hist_brks = hist_brks, lpi = lpi, density = density))
      }else{
        stop("Something went wrong")
      }
    }
    
    # creates the densities and the counts over bins (only ny is relevant) - doing hist2d
    .dens_histCounts <- function(progind, barr, ny, lpi = NULL, density = TRUE){
      if(is.null(lpi)) lpi_tmp <- nrow(progind)
      else lpi_tmp <- lpi
      hist.internal <- gplots::hist2d(progind[,1:2], ncol=2, nrow=lpi_tmp, nbins=c(ny, ny), show=F)
      cnts <- matrix(0, nrow=ny, ncol=(.lt(barr) + 1))
      for (i in 1:ncol(cnts)) {
        if (i==1) {
          ncls <- 1
          ncle <- which.min(abs(hist.internal$y - barr[i]))
        } else if (i == (.lt(barr) + 1) ) {
          ncls <- which.min(abs(hist.internal$y - barr[i-1])) 
          ncle <- ny
        } else {
          ncls <- which.min(abs(hist.internal$y - barr[i-1]))
          ncle <- which.min(abs(hist.internal$y - barr[i]))
        }
        ## ColSums doesn't work if ncls==ncle
        for (j in 1:ny) cnts[j, i] <- sum(hist.internal$counts[c(ncls:ncle), j]) # here is where the y is summed up
      }
      if(density){
        return(apply(cnts, 2, function(x) (x*1.0)/sum(x)))
      }else{
        return(cnts)
      }
    }
    
    .horiz_hist <- function(progind, barr, hist_brks, lpi = NULL, density = TRUE){
      if(is.null(lpi)) lpi_tmp <- nrow(progind)
      else lpi_tmp <- lpi
      brks <- c(1, which(progind[,1] %in% (barr)) + 1, lpi_tmp + 1)
      ltbrks <- .lt(brks) - 1
      cnts <- NULL
      for(jij in 1:ltbrks) {
        cnts <- cbind(cnts, hist(c(progind[(brks[jij]):(brks[jij + 1] - 1), 2]), plot = F, breaks = hist_brks, include.lowest = T, right = F)$counts)
      }
      if(density){
        return(apply(cnts, 2, function(x) (x*1.0)/sum(x)))
      }else{
        return(cnts)
      }
    }  
  }
  # F EXTRAS ======================================================================================
  if(F){
    require(ggplot2)
    ######################### evaluating an automatic way to select the number of cluster -> grid search ##########################
    
    # initialization of the vars 
    sco.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    sco.rnd.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    rownames(sco.mat) <- rownames(sco.rnd.mat) <- paste0('ncl.', ncl.vec, '.i')
    colnames(sco.mat) <- colnames(sco.rnd.mat) <- paste0('ncl.', ncl.vec, '.j')
    
    # call for barriers -> more than needed (it will be cross scored)
    tmp <- CampaRi::nSBR(data = file.pi, n.cluster = 15,
                                       comb_met = c('MIC'),
                                       unif.splits = unique(round(seq(5, 100, length.out = 40))),  
                                       pk_span = 150, ny = 60, plot = T,
                                       silent = F, dbg_nSBR = F, return_plot = F)
    ncl.vec <- 2:(length(tmp$barriers) + 1)
    
    # main loop over clusters I ----------------------------------------------------------------------------------
    for(ncl.i in ncl.vec){
      lnclv <- length(ncl.vec)
      pwa <- which(ncl.vec == ncl.i)
      cat('NCI: Doing cluster', ncl.i, '(', (pwa - 1)*lnclv + 1 , '-', pwa*lnclv, '/', lnclv^2, ')\n')
      
      
      # extraction of the annotation from barriers (numbering not relevant)
      ref.vec <- .vec_from_barriers(bar.vec = c(tmp$barriers[1:(ncl.i-1)], 3000))
      
      # internal loop over clusters J ---------------------------------------------------------------------------
      for(ncl.j in ncl.vec){
        cat('NCJ: Doing cluster', ncl.j, '(', which(ncl.vec == ncl.j), '/', lnclv, ')\n')
        
        # scoring
        ref_sc <- CampaRi::score_sapphire(the_sap = file.pi, ann = ref.vec, 
                                          manual_barriers = tmp$barriers[1:(ncl.j-1)], silent = T)
        
        # final assignment
        sco.mat[paste0('ncl.', ncl.i, '.i'), paste0('ncl.', ncl.j, '.j')] <- ref_sc$score.out
        # sco.rnd.mat[paste0('ncl.', ncl.i), paste0('ncl.', ncl.j)] <- res2
      }
    }
    
    # Plotting results for visual inspection
    ggplot_tiles(mat = sco.mat, labx = 'Clusters', laby = 'Clusters', legtit = 'ARI') + geom_text(label = round(unlist(c(sco.mat)),2), col = 'white')
    
    # best scenario / selection of the number of clusters - using the parameter search (below)
    CampaRi::nSBR(data = file.pi, n.cluster = 5,
                  comb_met = c('MIC'),
                  unif.splits = unique(round(seq(5, 100, length.out = 40))),  
                  pk_span = 350, ny = 60, plot = T,
                  silent = F, dbg_nSBR = F, return_plot = F)
    
    # --------------------------- functions ---------------------------------
    
    # tile plot from matrix
    ggplot_tiles <- function(mat, labx = '', laby = '', legtit = '', normalize = FALSE, return_plot = FALSE){
      
      stopifnot(is.character(labx), is.character(laby), is.character(legtit))
      stopifnot(is.logical(normalize), is.logical(return_plot))
      
      melted <- reshape2::melt(as.matrix(mat))
      if(normalize) melted[,3] <- .normalize(melted[,3])
      
      itplots <- ggplot(melted, aes(x = Var2, y = Var1)) + theme_minimal() +
                      geom_raster(aes(fill=value)) + 
        scale_fill_gradientn(legtit, colours = c('black', 'darkred')) + 
        xlab(labx) + ylab(laby) 
      
      if(return_plot) return(itplots)
      else print(itplots)
    }
    
    
    # create annotation vector from barrier vector # not used at the moment
    .vec_from_barriers <- function(bar.vec, label.vec = NULL, reorder.index = FALSE){ # , end.point = NULL (I can put it inside the bar.vec)
      
      # bar.vec must be the position on the PI!!! not the lengths of stretches
      stopifnot(all(bar.vec > 1))
      
      # sorting
      sorted.bar.vec <- sort(bar.vec, index.return=TRUE) # should I consider also the indexing? I don't think so
      bar.vec <- sorted.bar.vec$x 
      if(reorder.index){
        if(!is.null(label.vec)) label.vec <- label.vec[sorted.bar.vec$ix]
        else label.vec <- sorted.bar.vec$ix
      }
      
      # if not defined define the label vector
      if(is.null(label.vec)) label.vec <- 1:length(bar.vec)
      stopifnot(all(1:length(label.vec) == sort(label.vec))) # no gaps allowed
      stopifnot(length(label.vec) == length(bar.vec))
      
      # generate the diff bector
      bar.vec.diff <- c(bar.vec[1], diff(bar.vec))
      stopifnot(sum(bar.vec.diff) == bar.vec[length(bar.vec)]) # check the right summing up!
      
      # final loop
      return(unlist(sapply(1:length(label.vec), function(x) rep(label.vec[x], bar.vec.diff[x]))))
    }
    
    # repear until the right number of clusters is found
    repeat.until.cl <- function(ncl.x, max_rounds = 10, h.splits.t = 20,
                                span.t = 500, ny.t = 30, silent = F, plot = F){
      round <- 1
      repeat {
        sspplliitts <- unique(round(seq(5, 100, length.out = h.splits.t)))
        tmp <- CampaRi::nSBR(data = file.pi, n.cluster = ncl.x,
                                                  comb_met = c('MIC'),
                                                  unif.splits = sspplliitts,  
                                                  pk_span = span.t, ny = ny.t, plot = plot,
                                                  silent = silent, dbg_nSBR = F, return_plot = F)
        if(length(tmp$barriers) > (ncl.x - 2)){
          if(!silent) cat('Found', length(tmp$barriers), 'barriers over', ncl.x - 1, '\n')
          fnd <- TRUE
          break
        }else{
          if(!silent) cat('Found', length(tmp$barriers), 'barriers instead of', ncl.x - 1, '\n')
          h.splits.t <- h.splits.t + 10
          span.t <- max(100, span.t - 50)
          ny.t <- ny.t + 10
          round <- round + 1
          if(round > max_rounds){
            fnd <- FALSE
            break
          }
        }
      }
      return(list('found' = fnd, 'tmp' = tmp, 'h.splits.t' = h.splits.t, 'span.t' = span.t, 'ny.t' = ny.t, 'round' = round))
    }
    
    # --------------------------- end functions ---------------------------------
    
    # This method is looking for the barriers changing the data. Is it necessary?
    # initialization of the vars 
    ncl.vec <- 2:6
    sco.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    sco.rnd.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    h.split.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    span.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    ny.mat <- data.frame(array(NA, dim = c(length(ncl.vec), length(ncl.vec))))
    rownames(sco.mat) <- rownames(span.mat) <- rownames(sco.rnd.mat) <- rownames(h.split.mat) <- rownames(ny.mat) <- paste0('ncl.', ncl.vec, '.i')
    colnames(sco.mat) <- colnames(span.mat) <- colnames(sco.rnd.mat) <- colnames(ny.mat) <- colnames(h.split.mat) <- paste0('ncl.', ncl.vec, '.j')
    # main loop over clusters I ----------------------------------------------------------------------------------
    for(ncl.i in ncl.vec){
      cat('NCI: Doing', ncl.i, '(', which(ncl.vec == ncl.i), '/', length(ncl.vec), ')\n')
      
      # first call for barriers
      temp1 <- repeat.until.cl(ncl.x = ncl.i,  max_rounds = 15, plot = F,
                               h.splits.t = 10, span.t = 500, ny.t = 30, silent = T)

      if(temp1$found) cat('Found the following barriers:', temp1$tmp$barriers, '\n')
      else stop('No correct number!!')
      
      # extraction of the annotation from barriers (numbering not relevant)
      ref.vec <- vec.from.barriers(bar.vec = temp1$tmp$barriers[1:(ncl.i-1)])
      
      # internal loop over clusters J ---------------------------------------------------------------------------
      for(ncl.j in ncl.vec){
        cat('NCJ: Doing', ncl.j, '(', which(ncl.vec == ncl.j), '/', length(ncl.vec), ')\n')
        
        # second call for barriers
        temp2 <- repeat.until.cl(ncl.x = ncl.j,  max_rounds = 15, plot = F,
                                 h.splits.t = 10, span.t = 500, ny.t = 30, silent = T)
        
        if(temp2$found) cat('Found the following barriers:', temp2$tmp$barriers, '\n')
        else stop('No correct number!!')
        
        # scoring
        ref_sc <- CampaRi::score_sapphire(the_sap = file.pi, ann = ref.vec, 
                                          manual_barriers = temp2$tmp$barriers[1:(ncl.j-1)], silent = T)
        
        # final assignment
        h.split.mat[paste0('ncl.', ncl.i, '.i'), paste0('ncl.', ncl.j, '.j')] <- temp2$h.splits.t
        span.mat[paste0('ncl.', ncl.i, '.i'), paste0('ncl.', ncl.j, '.j')] <- temp2$span.t
        ny.mat[paste0('ncl.', ncl.i, '.i'), paste0('ncl.', ncl.j, '.j')] <- temp2$ny.t
        sco.mat[paste0('ncl.', ncl.i, '.i'), paste0('ncl.', ncl.j, '.j')] <- ref_sc$score.out
        sco.rnd.mat[paste0('ncl.', ncl.i), paste0('ncl.', ncl.j)] <- res2
      }
    }
    # apply(sco.mat, 2, mean) 
    # apply(sco.mat, 1, mean) 
    
    
    ggplot_tiles(mat = h.split.mat, labx = 'Clusters', laby = 'Clusters', legtit = 'ARI') + geom_text(label = round(unlist(c(h.split.mat)),2), col = 'white')
    ggplot_tiles(mat = span.mat, labx = 'Clusters', laby = 'Clusters', legtit = 'ARI') + geom_text(label = round(unlist(c(span.mat)),2), col = 'white')
    ggplot_tiles(mat = ny.mat, labx = 'Clusters', laby = 'Clusters', legtit = 'ARI') + geom_text(label = round(unlist(c(ny.mat)),2), col = 'white')
    
    
  ######################### evaluating the fluctuation and randomicity of the score ##########################
    # just curiosity - entropy considerations
    df.test <- data.frame(a = rnorm(n = 1000, mean = 0, sd = 1), b = rnorm(n = 1000, mean = 0, sd = 1), c = rnorm(n = 1000, mean = 6, sd = 1))
    ggplot(data = df.test) + theme_classic() +
      geom_freqpoly(mapping = aes(a), binwidth = 0.4, col = 'darkred') +
      geom_freqpoly(mapping = aes(b), binwidth = 0.4, col = 'darkblue') +
      geom_freqpoly(mapping = aes(c), binwidth = 0.4) 
    
    .SHEN.hist(df.test$a, 100); .SHEN.hist(df.test$b, 100)
    .SHEN.hist(df.test$a, 100) + .SHEN.hist(df.test$b, 100)
    .SHEN.hist(df.test$a, 100) + .SHEN.hist(df.test$c, 100)
    .SHEN.hist(c(df.test$a, df.test$b), 200)
    .SHEN.hist(c(df.test$a, df.test$c), 200)

    # tring to make a sense if there is 
    slide <- seq(-100, 100, 5)
    binning <- 300
    multivar.SHEN <- sapply(X = slide, FUN = function(x) .SHEN.hist(c(df.test$a, df.test$b + x), binning))
    sum.SHEN <- sapply(X = slide, FUN = function(x) .SHEN.hist(df.test$a, binning) + .SHEN.hist(df.test$b + x, binning))
    df.t <- data.frame('diff' = .normalize(sum.SHEN - multivar.SHEN), 'sSHEN' = .normalize(sum.SHEN), 'mSHEN' = .normalize(multivar.SHEN))
    ggplot(data = df.t, mapping = aes(x = slide)) + theme_classic() +
      geom_line(mapping = aes(y = diff, col = 'diff')) + 
      geom_line(mapping = aes(y = sSHEN, col = 'Entropy sum'), linetype = 'dotted', size = 1) + 
      geom_line(mapping = aes(y = mSHEN, col = 'Dist sum')) + 
      geom_point(x = which.max(df.t$diff), y = max(df.t$diff)) + 
      geom_point(x = 0, y = 0, col = 'red')
    
    # entropy as a function of the binning
    binning <- seq(1, 1000, 10)
    a.shen.line <- sapply(X = binning, FUN = function(x) .SHEN.hist(df.test$a, x))
    b.shen.line <- sapply(X = binning, FUN = function(x) .SHEN.hist(df.test$b, x))
    c.shen.line <- sapply(X = binning, FUN = function(x) .SHEN.hist(df.test$c, x))
    df.shen.line <- data.frame(a.shen.line, b.shen.line, c.shen.line)  
    ggplot(data = df.shen.line, mapping = aes(x = binning)) + theme_classic() +
      geom_line(mapping = aes(y = a.shen.line), col = 'darkred') +
      geom_line(mapping = aes(y = b.shen.line), col = 'darkblue') +
      geom_line(mapping = aes(y = c.shen.line)) 
  }
})
.SHEN.hist <- function(x, brks){
  hist.int <- hist(x, breaks = brks, plot=FALSE)
  dens <- hist.int$density
  return(.myShEn(dens))
}
.myShEn <- function(x){
  x2 <- replace(x = x, list = which(x == 0), values = 1)
  return(-sum(x2*log(x2)))
}
.normalize <- function(x, xmax = NULL, xmin = NULL) {
  # if(.isSingleElement(x)) return(1)
  if(is.null(xmax)) xmax <- max(x)
  if(is.null(xmin)) xmin <- min(x)
  if(xmin == xmax) return(x/xmax)
  else return((x*1.0 - xmin)/(xmax - xmin))
}





