context('adjl_from_adjmat')

test_that('Test adjl_from_adjmat', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  ev_it(a <- adjl_from_adjmat(matrix(rnorm(1000), nrow = 10, ncol = 100)), NA)
  
})

