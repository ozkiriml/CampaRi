context('NetSAP')

test_that('tests for NetSAP', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  # creating the oscillatory system - 3 states
  tot_time <- 10000
  ttt <- 1:tot_time
  whn <- sample(ttt, size = 15)
  wht <- sample(0:3, size = 16, replace = T)
  tjum <- c(0, sort(whn), tot_time)
  diff_tjum <- diff(tjum)
  mat_fin <- array(0, dim = c(tot_time, 3))
  annot <- array(NA, dim = tot_time)
  for(i in 1:length(wht)){
    wherr <- (tjum[i]+1):tjum[i+1]
    if(wht[i]==1){
      mat_fin[wherr, 1] <- sin(wherr/5 + 0.3)
      mat_fin[wherr, 2] <- sin(wherr/5) + 0.1    
    } else if(wht[i]==2){
      mat_fin[wherr, 3] <- sin(wherr/5)
      mat_fin[wherr, 2] <- sin(wherr/5 + 0.5) + 0.1    
    } else if(wht[i]==3){
      mat_fin[wherr, 3] <- sin(wherr/5 + 0.1)
      mat_fin[wherr, 1] <- sin(wherr/5) + 0.1    
    }
    annot[wherr] <- rep(wht[i], diff_tjum[i]) 
  }
  mat_fin <- mat_fin + matrix(rnorm(tot_time*3)/5, nrow = tot_time, ncol = 3)
  if(plt_stff){
    plot_traces(t(mat_fin[250:500,]), show_every = 2)
    plot_traces(t(mat_fin), show_every = 2)
    plot(annot, pch = ".")
  }
  
  
  # NetSAP -------------------------------------------------------------------------------------------------------------------------------
  data.table::fwrite(as.data.frame(mat_fin), file = "test_trj.tsv", sep = " ")
  ev_it(NetSAP(data_file = "test_trj.tsv", ncores_camp = 1, ncores_net = 1, folder = ".", trjsfile = "preproc_trj", base.name = "base_name",
         wii = 40, metho = "minkowski", princ_comps = 0, size_perc_search = 2000, qt = c(0.00015, 0.14),
         pi.start = 1, leaves.tofold = 5, search.attempts = 1000, state.width = 1000, print_mem = TRUE, silent = silent), s = silent)
  ev_it(SST_campari(trj = as.matrix(mat_fin), silent = silent), s = silent) 
  if(plt_stff){
    sapphire_plot("PI_output_basename_pistart1.dat", timeline = T, ann_trace = annot)
    sapphire_plot("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", timeline = T, ann_trace = annot)
  }
  out_bar <- nSBR("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", ny = 100, n.cluster = 4, plot = plt_stff, silent = silent)
  out_bar2 <- nSBR("PI_output_basename_pistart1.dat", ny = 100, n.cluster = 4, plot = plt_stff, silent = silent)
  out_sc1 <- score_sapphire("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", ann = as.numeric(annot + 1), manual_barriers = out_bar$barriers_sel, plot_pred_true_resume = plt_stff, silent = silent)
  out_sc2 <- score_sapphire("PI_output_basename_pistart1.dat", ann = as.numeric(annot + 1), manual_barriers = out_bar2$barriers_sel, plot_pred_true_resume = plt_stff, silent = silent)
  # expect_true(out_sc1$score.out > out_sc2$score.out)
  
  loop_file_rm(c("test_trj.tsv",
                 "basename.log", "basename.tsv", "PI_output_basename_pistart1.dat",
                 "base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.log", "base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.tsv", "PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat"))
})





